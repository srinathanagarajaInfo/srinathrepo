/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify18TheFunctionalityOfAddAttachmentPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.loader2.loc")
	private QAFExtendedWebElement loader2;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
	    
   }

public void WaitForLoader()
{
	loader.waitForNotVisible();
}

public void clickOnAttachBrowseButton() throws Exception
{

	driver.findElement(By.xpath("//div[@class='attachment_dotted_box m-b-15 flot-left m-r-10 cursor-pointer']")).click();
	QAFTestBase.pause(3000);
	
}

public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}

public void clickOnTravelTab()
{
	loader.waitForNotVisible();
	traveltabTab.waitForVisible();
	traveltabTab.click();
}

public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}

}
   
	


