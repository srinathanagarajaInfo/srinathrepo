
package com.qmetry.qaf.example.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify6LogoutFunctionalityPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
   public static Actions action;
   public static JavascriptExecutor js;

	
	@FindBy(locator = "login.managertab")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.logoutButton")
	private QAFWebElement LogoutButton;
//	
//	
//	@FindBy(locator = "login.TravelTab")
//	private QAFExtendedWebElement traveltabTab;
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}
  
//public void ValidateLogoutButton()
//{
//	driver.findElement(By.xpath("//input[@id='username']")).sendKeys("ashish.biradar1");
//	driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Test@1234");
//	QAFWebElement LogintButtonTab = driver.findElement(By.xpath("//button[@type='submit']"));
//	js.executeScript("arguments[0].click();", LogintButtonTab);	
//}

  public void ClickOnTravelAndTravelRequestTab()
  {
	 
//	 driver.findElement("//li[@class='active parent_li ng-scope left_menu_open_section']//span[@class='left_menu_item_level1 ng-binding'][contains(text(),'Travel')]").waitForVisible();
//	 QAFWebElement Travel = driver.findElement("//li[@class='active parent_li ng-scope left_menu_open_section']//span[@class='left_menu_item_level1 ng-binding'][contains(text(),'Travel')]");
//	 action = new Actions(driver);
//	 action.moveToElement(Travel).click().build().perform();
//	 driver.findElement("//li[@class='active parent_li ng-scope left_menu_open_section']//a[@class='left_menu_item_level2 ng-binding ng-scope'][contains(text(),'Travel Requests')]").waitForVisible();
//	 QAFWebElement TravelRequestTab = driver.findElement("//li[@class='active parent_li ng-scope left_menu_open_section']//a[@class='left_menu_item_level2 ng-binding ng-scope'][contains(text(),'Travel Requests')]");
//	 action = new Actions(driver);
//	 action.moveToElement(TravelRequestTab).click().build().perform();
//	 
//	 js = (JavascriptExecutor)driver;
//	 js.executeScript("arguments[0].click();", Travel);
//	 js.executeScript("arguments[0].click();", TravelRequestTab);
	 
	   
		QAFWebElement Travel = driver.findElement(By.xpath("(//span[@title='Travel'])[2]"));
		action.moveToElement(Travel).click().build().perform();
		QAFTestBase.pause(2000);
		
		QAFWebElement TravelRequestTab = driver.findElement(By.xpath("(//a[contains(text(),'Travel Requests')])[3]"));
		action.moveToElement(TravelRequestTab).click().build().perform();
		QAFTestBase.pause(2000);

  }
  
  public void clickOnTravelPurpose()
  {
	  driver.findElement("TravelPurposeColumn").waitForVisible();
	  driver.findElement("TravelPurposeColumn").click();
	  
  }

  public void userShouldVerifyApproveRejectBackButtons()
  {
	  driver.findElement("login.ApproveButton").verifyPresent();
	  driver.findElement("login.RejectButton").verifyPresent();
	  driver.findElement("login.BackButton").verifyPresent();
  }
  
  public void clickOnLogoutButton()
  {
	    waitForPageToLoad();
//		QAFWebElement LogoutButtonTab = driver.findElement(By.xpath("//div[@id='logout_btn1']"));
//		js.executeScript("arguments[0].click();", LogoutButtonTab);
		
		QAFWebElement LogoutButtonTab = driver.findElement(By.xpath("//div[@id='logout_btn1']"));
		action = new Actions(driver);
		action.moveToElement(LogoutButtonTab).click().build().perform();
		
  }

}
   
	


