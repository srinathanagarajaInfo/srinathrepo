/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify10DetailOfBrightSparkPage extends WebDriverBaseTestPage<WebDriverTestPage>

{
	
	public static JavascriptExecutor js;

	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.mgrusername.loc")
	private QAFWebElement mgrusernametxt;
	
	@FindBy(locator = "login.dmusername.loc")
	private QAFWebElement dmusernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.dmpassword.loc")
	private QAFExtendedWebElement dmpasswordtxt;
	
	@FindBy(locator = "login.mgrpassword.loc")
	private QAFExtendedWebElement mgrpasswordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.logoutButton.loc")
	private QAFExtendedWebElement logoutButtonTab;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
		
	@FindBy(locator = "login.RandR.loc")
	private QAFExtendedWebElement loginRandRTab;
	
	@FindBy(locator = "login.Nominate.loc")
	private QAFExtendedWebElement loginNominateTab;
	
	@FindBy(locator = "login.BrightSpark.loc")
	private QAFExtendedWebElement loginBrightSparkTab;
	
	@FindBy(locator = "login.SelectCard.loc")
	private QAFExtendedWebElement loginSelectCardTab;
	
//	##########################
	
	@FindBy(locator = "login.Project.loc")
	private QAFExtendedWebElement loginProjectTab;
	
	@FindBy(locator = "login.ProjectNestTest.loc")
	private QAFExtendedWebElement loginProjectNestTestTab;
	
	@FindBy(locator = "login.Nominee.loc")
	private QAFExtendedWebElement loginNomineeTab;
	
	@FindBy(locator = "login.NomineeAsVirat.loc")
	private QAFExtendedWebElement loginNomineeAsViratTab;
	
//	############################
	
	@FindBy(locator = "login.ChallengingSituationsFaced.loc")
	private QAFExtendedWebElement loginChallengingSituationsFacedTab;
	
	@FindBy(locator = "login.SolutionsProvided.loc")
	private QAFExtendedWebElement loginSolutionsProvidedTab;
	
	@FindBy(locator = "login.BenefitsAccrued.loc")
	private QAFExtendedWebElement loginBenefitsAccruedTab;
	
	@FindBy(locator = "login.RationaleForNomination.loc")
	private QAFExtendedWebElement loginRationaleForNominationTab;
	
    @FindBy(locator = "login.Post.loc")
    private QAFExtendedWebElement loginPostTab;
    	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	@FindBy(locator = "login.RRrequests.loc")
	private QAFExtendedWebElement RRrequestsTab;
	
	@FindBy(locator = "login.RewardName.loc")
	private QAFExtendedWebElement RewardNamedTab;
	
	@FindBy(locator = "login.approve.loc")
	private QAFExtendedWebElement approveTab;
	
//###########
	
	@FindBy(locator = "login.NomineeLable.loc")
	private QAFExtendedWebElement NomineeLable;
	
	@FindBy(locator = "login.ProjectLable.loc")
	private QAFExtendedWebElement ProjectLable;
	
	@FindBy(locator = "login.RewardNameLable.loc")
	private QAFExtendedWebElement RewardNameLable;
	
	@FindBy(locator = "login.PostedDateLable.loc")
	private QAFExtendedWebElement PostedDateLable;
	
	@FindBy(locator = "login.NominatedByLable.loc")
	private QAFExtendedWebElement NominatedByLable;
	
	@FindBy(locator = "login.ManagerLable.loc")
	private QAFExtendedWebElement ManagerLable;
	
	@FindBy(locator = "login.ManagerStatusLable.loc")
	private QAFExtendedWebElement ManagerStatusLable;
	
	@FindBy(locator = "login.HRStatusLable.loc")
	private QAFExtendedWebElement HRStatusLable;
	
	@FindBy(locator = "login.BrightSparkRejected.loc")
	private QAFExtendedWebElement BrightSparkRejectedTab;
	
	@FindBy(locator = "login.BrightSparkPending.loc")
	private QAFExtendedWebElement BrightSparkPendingTab;
	
	@FindBy(locator = "login.BrightSparkHRApprove.loc")
	private QAFExtendedWebElement BrightSparkHRApproveTab;
	
	
	
//###########	
	
	@FindBy(locator = "login.RewardCardImage.loc")
	private QAFExtendedWebElement RewardCardImage;
	
	@FindBy(locator = "login.BackButton.loc")
	private QAFExtendedWebElement BackButton;
	
	
//	###########
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
   public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
 
   }

   public void loginWithManagerID(String username, String password) {
	    mgrusernametxt.waitForVisible();
	    mgrusernametxt.sendKeys(username);
	    mgrpasswordtxt.waitForVisible();
	    mgrpasswordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
	    
	    
  }

//   public void loginWithDeliveryManagerID(String username, String password) {
//	    dmusernametxt.waitForVisible();
//	    dmusernametxt.sendKeys(username);
//	    dmpasswordtxt.waitForVisible();
//	    dmpasswordtxt.sendKeys(password);
//	    loginButtonbtn.waitForVisible();
//	    loginButtonbtn.click();
//	    loader.waitForNotVisible();
//	    	    
// }
   
public void WaitForLoader()
   {
   	loader.waitForNotVisible();
   }

public void clickOnRandRTab()
{
	WaitForLoader();
	QAFWebElement loginRandRTab = driver.findElement(By.xpath("//span[contains(@title,'R & R')]"));	
	QAFTestBase.pause(2000);
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", loginRandRTab);

}

public void clickOnNominateTab()
{
	WaitForLoader();
	
	QAFWebElement loginNominateTab = driver.findElement(By.xpath("//span[contains(.,'Nominate')]"));
	Actions actions = new Actions(driver);
	actions.moveToElement(loginNominateTab).click().build().perform();
	QAFTestBase.pause(4000);
	
}

public void clickOnBrightSparkTab()
{
	WaitForLoader();
	QAFWebElement loginBrightSparkTab = driver.findElement(By.xpath(""
			+ ""));
	Actions actions = new Actions(driver);
	actions.moveToElement(loginBrightSparkTab).click().build().perform();
	QAFTestBase.pause(4000);
		
}

public void clickOnSelectCardButton()
{

	WaitForLoader();
	QAFWebElement loginSelectCardTab = driver.findElement(By.xpath("//img[@src='https://neststag.infostretch.com:8446/nest_security_testing/nativefw/images/rewardrecognization/Bright_Spark/Card_1.jpg']"));
	js = (JavascriptExecutor)driver;
	js.executeScript("arguments[0].click();", loginSelectCardTab);
	QAFTestBase.pause(4000);
}

public void clickOnNomineeTab()
{
	
	WaitForLoader();
	driver.findElement(By.xpath("//div[contains(@class,'row m-t-20')]//div[3]//div[1]//div[2]//div[1]//div[1]//button[1]")).click();
	driver.findElement(By.xpath("//input[@placeholder='Search...']")).sendKeys("Virat Kohali");
	Actions action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();

}
public void clickOnProjectTab()
{
	WaitForLoader();
	driver.findElement(By.xpath("//*[contains(text(),'Select Project')]")).click();
	driver.findElement(By.xpath("//input[@placeholder='Search...']")).sendKeys("Nest Test");
	Actions action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();

}

public void enterTextChallengingSituationsFaced()
{
	loginChallengingSituationsFacedTab.waitForVisible();
	loginChallengingSituationsFacedTab.sendKeys("dsfsdfg");
}

public void enterTextSolutionsProvidedTab()
{
	loginSolutionsProvidedTab.waitForVisible();
	loginSolutionsProvidedTab.sendKeys("sdfgd");
}

public void enterTextBenefitsAccruedTab()
{
	loginBenefitsAccruedTab.waitForVisible();
	loginBenefitsAccruedTab.sendKeys("sdgf");
}

public void enterTextRationaleForNominationTab()
{
	loginRationaleForNominationTab.waitForVisible();
	loginRationaleForNominationTab.sendKeys("fgfg");
}

public void clickOnPostTab()
{
	loginPostTab.waitForVisible();
	QAFWebElement loginPostTab = driver.findElement(By.xpath("//button[contains(text(),'Post')]"));
	Actions actions = new Actions(driver);
	actions.moveToElement(loginPostTab).click().build().perform();
}

public void clickOnLogoutButton()
{
	logoutButtonTab.waitForVisible();
	logoutButtonTab.click();
}

public void clickOnManagerView()
{
	WaitForLoader();
	managertabTab.waitForVisible();
	managertabTab.click();
	QAFTestBase.pause(40000);
}

public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}

public void ClickRandRTabButton()
{
	WaitForLoader();
	QAFWebElement loginRandRTab = driver.findElement(By.xpath("(//span[contains(@title,'R & R')]//child::img[contains(@class,'settings_btn1')])[1]"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", loginRandRTab);	
}

public void clickOnRRrequestsTabButton()
{
	WaitForLoader();
	QAFWebElement RRrequestsTab = driver.findElement(By.xpath("//a[contains(text(),'R & R Requests')]"));	
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", RRrequestsTab);
	
}

public void clickOnBrightSparkRejectTab()
{
	WaitForLoader();
	QAFWebElement BrightSparkRejectTab = driver.findElement(By.xpath("//td[text()='Rejected']//parent::tr[@class='ng-scope']//td[text()='Bright Spark']"));	
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", BrightSparkRejectTab);
	
}

public void clickOnBrightSparkPendingTab()
{
	WaitForLoader();
	QAFWebElement BrightSparkPendingTab = driver.findElement(By.xpath("//td[@data-title='Manager Status' and text()='Pending']//parent::tr[@class='ng-scope']//td[contains(text(),'Bright Spark')]"));	
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", BrightSparkPendingTab);
	
}

public void clickOnBrightSparkHRApproveTab()
{
	WaitForLoader();
	QAFWebElement BrightSparkHRApproveTab = driver.findElement(By.xpath("//td[@data-title='HR Status' and text()='Pending']//preceding-sibling::td[@data-title='Manager Status' and text()='Approved']//parent::tr[@class='ng-scope']//td[contains(text(),'Bright Spark')]"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", BrightSparkHRApproveTab);
	
//	js.executeScript("arguments[0].scrollIntoView(true);", BrightSparkHRApproveTab);
//	Actions actions = new Actions(driver);
//	actions.moveToElement(BrightSparkHRApproveTab).click().build().perform();
//	js = (JavascriptExecutor) driver;
//	js.executeScript("arguments[0].scrollIntoView(true);", BrightSparkHRApproveTab);
//	js.executeScript("arguments[0].click();", BrightSparkHRApproveTab);
}

public void clickOnMyRRandMyRewards()
{
	WaitForLoader();
	QAFWebElement rrTab = driver.findElement(By.xpath("//span[@title='R & R' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu']"));
	QAFWebElement MyRRTab = driver.findElement(By.xpath("//span[contains(text(),'My R & R')]//parent::li[@class='ng-scope left_menu_close_section']"));
	QAFWebElement MyRewardsTab = driver.findElement(By.xpath("//a[contains(text(),'My Rewards')]//parent::li[@class='ng-scope left_menu_close_section']"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", rrTab);
	js.executeScript("arguments[0].click();", MyRRTab);
	js.executeScript("arguments[0].click();", MyRewardsTab);
	
}

public void clickOnRRMenuBar()
{
	WaitForLoader();
	QAFWebElement TabRR = driver.findElement(By.xpath("//span[@title='R & R' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu']"));
	QAFWebElement TabNominate = driver.findElement(By.xpath("//span[text()='Nominate' and @class='left_menu_item_level2 ng-binding ng-scope']"));
	QAFWebElement TabBrighSpark = driver.findElement(By.xpath("//a[text()='Bright Spark' and @class='left_menu_item_level3 ng-binding']"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", TabRR);
	js.executeScript("arguments[0].click();", TabNominate);
	js.executeScript("arguments[0].click();", TabBrighSpark);
}

public void clickOnSecTestTab()
{
	WaitForLoader();
	driver.findElement(By.xpath("//button[@class='expand_row_btn m-r-10']")).click();
}

public void clickOnApproveTab()
{
	WaitForLoader();
	QAFWebElement ApproveTab = driver.findElement(By.xpath("//button[contains(text(),' Approve')]"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", ApproveTab);
	
	Set <String>handles = driver.getWindowHandles();//To handle multiple windows
	Iterator<String> it = handles.iterator();
	String ParentWin = it.next();
	String ChildWin = it.next();
	driver.switchTo().window(ChildWin);
	QAFTestBase.pause(3000);
	driver.findElement(By.xpath("//textarea[@ng-model='approve_rejectRnRCtrl.comment']")).sendKeys("Good Job");
	driver.findElement(By.xpath("//button[@type='button']")).click();
	
	
}

public void verifyNomineeLable()
{
	WaitForLoader();
	NomineeLable.waitForVisible();
}

public void verifyHRStatusLable()
{
	WaitForLoader();
	HRStatusLable.waitForVisible();
}

public void verifyManagerStatusLable()
{
	WaitForLoader();
	ManagerStatusLable.waitForVisible();

}

public void verifyManagerLable()
{
	WaitForLoader();
	ManagerLable.waitForVisible();

}

public void verifyNominatedByLable()
{
	WaitForLoader();
	NominatedByLable.waitForVisible();
}

public void verifyProjectLable()
{
	WaitForLoader();
	ProjectLable.waitForVisible();
}

public void verifyRewardNameLable()
{
	WaitForLoader();
	RewardNameLable.waitForVisible();
}

public void verifyPostedDateLable()
{
	WaitForLoader();
	PostedDateLable.waitForVisible();
}

public void verifyRewardCardImage()
{
	RewardCardImage.waitForVisible();
}

public void verifyBackButton()
{
	BackButton.waitForVisible();
}

}
   
