
package com.qmetry.qaf.example.steps;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify4TheFunctionalityOfTravelRequestSectionPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
   public static Actions action;
   public static JavascriptExecutor js;

	
	@FindBy(locator = "login.ApproveButton")
	private QAFExtendedWebElement ApproveButton;
	
	@FindBy(locator = "login.RejectButton")
	private QAFExtendedWebElement RejectButton;
	
	@FindBy(locator = "login.BackButton")
	private QAFExtendedWebElement BackButton;
	
	@FindBy(locator = "login.TravelTab")
	private QAFExtendedWebElement TravelTab;
	
	@FindBy(locator = "login.TravelRequestsTab")
	private QAFExtendedWebElement TravelRequestsTab;
	
	@FindBy(locator = "login.TravelPurposeColumn")
	private QAFExtendedWebElement TravelPurposeColumn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
		
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }

  public void WaitForLoader()
  {
  	loader.waitForNotVisible();
  }

  public void ClickOnTravelTab()
  {
	    WaitForLoader();
		QAFWebElement TravelTabLink = driver.findElement("(//span[contains(@title,'Travel')]//child::img[contains(@class,'settings_btn1')])[2]");
//		action = new Actions(driver);
//		action.moveToElement(TravelTab).click().build().perform();
//		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();", TravelTab);

//		TravelRequestsTab.waitForVisible();
//		QAFWebElement TravelRequestsTab = driver.findElement("login.TravelRequestsTab");
//		action = new Actions(driver);
//		action.moveToElement(TravelRequestsTab).click().build().perform();
//	  
  }
  
  public void clickOnTravelRequests()
  {
		WaitForLoader();
		QAFWebElement TravelRequestsTabLink = driver.findElement("//li[@class='active parent_li ng-scope left_menu_open_section']//a[@class='left_menu_item_level2 ng-binding ng-scope'][contains(text(),'Travel Requests')]");
		action = new Actions(driver);
		action.moveToElement(TravelRequestsTab).click().build().perform();
  }
  

  
  public void clickOnTravelPurpose()
  {
	  TravelPurposeColumn.waitForVisible();
	  TravelPurposeColumn.click();
  }
  
  public void userShouldVerifyApproveRejectBackButtons()
  {
	  driver.findElement("login.ApproveButton").verifyPresent();
	  driver.findElement("login.RejectButton").verifyPresent();
	  driver.findElement("login.BackButton").verifyPresent();
  }

}
   
	


