/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify20TheApprovalProcessOfPatOnBackPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	public static JavascriptExecutor js;
	
//	@FindBy(locator = "login.username.loc")
//	private QAFWebElement usernametxt;
//	
//	@FindBy(locator = "login.password.loc")
//	private QAFExtendedWebElement passwordtxt;
//	
//	@FindBy(locator = "login.loginButton.loc")
//	private QAFExtendedWebElement loginButtonbtn;
//	
//	@FindBy(locator = "login.loader.loc")
//	private QAFExtendedWebElement loader;
//	
//	@FindBy(locator = "login.managertab.loc")
//	private QAFExtendedWebElement managertabTab;
//	
//	@FindBy(locator = "login.loader2.loc")
//	private QAFExtendedWebElement loader2;
//	
//	@FindBy(locator = "login.traveltab.loc")
//	private QAFExtendedWebElement traveltabTab;
//	
//	@FindBy(locator = "login.viewtab.loc")
//	private QAFExtendedWebElement viewtabTab;
	
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
//public void clickOnLoginPage(String username, String password) {
//	    usernametxt.waitForVisible();
//	    usernametxt.sendKeys(username);
//	    passwordtxt.waitForVisible();
//	    passwordtxt.sendKeys(password);
//	    loginButtonbtn.waitForVisible();
//	    loginButtonbtn.click();
//	    loader.waitForNotVisible();
//	    
//	    
//   }
//
//public void WaitForLoader()
//{
//	loader.waitForNotVisible();
//}

public void clickOnRRPatOnBackBar()
{
//	WaitForLoader();
	QAFWebElement TabRR = driver.findElement(By.xpath("//span[@title='R & R' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu']"));
	QAFWebElement TabNominate = driver.findElement(By.xpath("//span[text()='Nominate' and @class='left_menu_item_level2 ng-binding ng-scope']"));
	QAFWebElement TabPatOnBack = driver.findElement(By.xpath("//a[text()='Pat On Back' and @class='left_menu_item_level3 ng-binding']"));
	js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", TabRR);
	js.executeScript("arguments[0].click();", TabNominate);
	js.executeScript("arguments[0].click();", TabPatOnBack);

}

public void clickOnSelectPatOnBackCardButton()
{
	
//	WaitForLoader();
	QAFWebElement SelectPatOnBackCardButton= driver.findElement(By.xpath("//img[@ng-src='https://neststag.infostretch.com:8446/nest_security_testing/nativefw/images/rewardrecognization/Pat_On_Back/Card_4.jpg']"));
	js = (JavascriptExecutor)driver;
	js.executeScript("arguments[0].click();", SelectPatOnBackCardButton);
	QAFTestBase.pause(4000);
}

public void userEnterKeyContributionByNominee()
{
	QAFWebElement EnterKeyContributionByNominee = 	driver.findElement(By.xpath("//textarea[@placeholder='Enter Contributions']"));
	EnterKeyContributionByNominee.sendKeys("NA");
}
//
//
//public void clickOnManagerView()
//{
//	managertabTab.waitForVisible();
//	managertabTab.click();
//}
//
//public void WaitForLoader2()
//{
//	loader2.waitForNotVisible();
//}
//
//public void clickOnTravelTab()
//{
//	loader.waitForNotVisible();
//	traveltabTab.waitForVisible();
//	traveltabTab.click();
//}
//
//public void clickOnlViewTab()
//{
//	viewtabTab.waitForVisible();
//	viewtabTab.click();
//}

}
   
	


