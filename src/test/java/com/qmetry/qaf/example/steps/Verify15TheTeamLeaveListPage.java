/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.TestPage;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify15TheTeamLeaveListPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
	public static Actions action;

	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.loader2.loc")
	private QAFExtendedWebElement loader2;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
	    
   }

public void WaitForLoader()
{
	loader.waitForNotVisible();
}

public void clickOnLeaveAndTeamLeaveList()
{
	  {
			
		    WaitForLoader();
			QAFWebElement LeaveTab = driver.findElement(By.xpath("(//span[@title='Leave' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu'])[2]"));
			action = new Actions(driver);
			action.moveToElement(LeaveTab).click().build().perform();
			QAFTestBase.pause(2000);
	
			QAFWebElement TeamLeaveListTab = driver.findElement(By.xpath("//a[contains(text(),'Team Leave List')]//parent::li[@class='ng-scope left_menu_close_section']"));
			action = new Actions(driver);
			action.moveToElement(TeamLeaveListTab).click().build().perform();
			QAFTestBase.pause(2000);
			
		}
}

public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}

public void clickOnTravelTab()
{
	loader.waitForNotVisible();
	traveltabTab.waitForVisible();
	traveltabTab.click();
}

public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}

}
   
	


