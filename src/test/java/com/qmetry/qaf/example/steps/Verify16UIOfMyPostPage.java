/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify16UIOfMyPostPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
	public static JavascriptExecutor js;
	public static Actions action;
	
	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
  
  public void clickOnPublisherAndMyPosts()
  {
	  {	
		    WaitForLoader();
//		    QAFWebElement PublisherTab =driver.findElement("login.PublisherTab");
			QAFWebElement PublisherTab = driver.findElement(By.xpath("(//span[@title='Publisher'])[2]"));
			action = new Actions(driver);
			action.moveToElement(PublisherTab).click().build().perform();
			QAFTestBase.pause(2000);
	
			
//			QAFWebElement MyPostsTab =driver.findElement("login.PublisherTab");
			QAFWebElement MyPostsTab = driver.findElement(By.xpath("//a[contains(text(),'My Posts') and @class='left_menu_item_level2 ng-binding ng-scope']"));
			action = new Actions(driver);
			action.moveToElement(MyPostsTab).click().build().perform();
			QAFTestBase.pause(2000);
			
		}
  }
  
  public void WaitForLoader()
{
	loader.waitForNotVisible();
}

}
   
	


