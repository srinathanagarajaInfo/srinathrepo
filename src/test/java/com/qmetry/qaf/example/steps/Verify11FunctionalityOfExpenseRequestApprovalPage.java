/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.TestPage;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify11FunctionalityOfExpenseRequestApprovalPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	public static JavascriptExecutor js;
	public static Actions action;
	
	
	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.Reimbursement.loc")
	private QAFExtendedWebElement ReimbursementTab;
	
	@FindBy(locator = "login.MyExpenseList.loc")
	private QAFExtendedWebElement MyExpenseListTab;
	
	@FindBy(locator = "login.NewExpense.loc")
	private QAFExtendedWebElement NewExpenseTab;
	
	@FindBy(locator = "login.NewExpenseTitle.loc")
	private QAFExtendedWebElement NewExpenseTitleTab;
	
	@FindBy(locator = "login.SelectProjectText.loc")
	private QAFExtendedWebElement SelectProjectTextTab;
	
	@FindBy(locator = "login.SelectExpenseText.loc")
	private QAFExtendedWebElement SelectExpenseTextTab;
	
	@FindBy(locator = "login.ExpenseAmountText.loc")
	private QAFExtendedWebElement ExpenseAmountTextTab;
	
	@FindBy(locator = "login.ExpenseSubmitButton.loc")
	private QAFExtendedWebElement ExpenseSubmitButton;
	
	@FindBy(locator = "login.ManagerReimbursement.loc")
	private QAFExtendedWebElement ManagerReimbursementTab;
	
	@FindBy(locator = "login.TeamsReimbursementList.loc")
	private QAFExtendedWebElement TeamsReimbursementListTab;
	
	@FindBy(locator = "login.TitleReimbursementListLink.loc")
	private QAFExtendedWebElement TitleReimbursementListLinkTab;
	
	@FindBy(locator = "login.ExpenseManagerTextBox.loc")
	private QAFExtendedWebElement ExpenseManagerTextBoxTab;
	
	@FindBy(locator = "login.ExpenseApproveButton.loc")
	private QAFExtendedWebElement ExpenseApproveButtonTab;
	
@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();   
   }

public void WaitForLoader()
{
	loader.waitForNotVisible();
}

public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}

public void clickOnExpenseReimbursementMyExpenseList()
{
	js = (JavascriptExecutor) driver;
	WaitForLoader();
	QAFWebElement ReimbursementTab = driver.findElement(By.xpath("//span[@title='Reimbursement']"));
	action = new Actions(driver);
	action.moveToElement(ReimbursementTab).click().build().perform();
	QAFTestBase.pause(2000);

	
	QAFWebElement MyExpenseListTab = driver.findElement(By.xpath("//a[text()='My Expense List']"));
	action = new Actions(driver);
	action.moveToElement(MyExpenseListTab).click().build().perform();
	QAFTestBase.pause(2000);
	
}
public void clickOnNewExpenseTab()
{
	WaitForLoader();
	NewExpenseTab.waitForVisible();
	QAFWebElement NewExpenseTab = driver.findElement(By.xpath("//button[text()='New Expense']"));
	action = new Actions(driver);
	action.moveToElement(NewExpenseTab).click().build().perform();
	QAFTestBase.pause(2000);
	
	driver.findElement(By.xpath("//input[@placeholder='Title']")).sendKeys("Travel New Expense");
//	########### Select Project
	driver.findElement(By.xpath("//tr[contains(@class,'ng-scope')]//td[2]//input[1]")).sendKeys("Nest Test");
	action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();
//	Select  Expense ######
	driver.findElement(By.xpath("//tr[contains(@class,'ng-scope')]//td[3]//input[1]")).sendKeys("1111");
	action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();

//	Select ExpenseAmountTextTab
	ExpenseAmountTextTab.waitForVisible();
	driver.findElement(By.xpath("//div[contains(@class,'input-group')]//input[contains(@placeholder,'')]")).sendKeys("1945");
	
	
}
	
	public void clickOnExpenseSubmitButton()
	{
	
	ExpenseSubmitButton.waitForVisible();
	QAFWebElement ExpenseSubmitButton = driver.findElement(By.xpath("//button[@class='btn_prim_1 ng-scope']"));
	js.executeScript("arguments[0].click();", ExpenseSubmitButton);
	
}
	
	public void logoutButton()
	{
		
		QAFWebElement logoutButton = driver.findElement(By.xpath("//div[@id='logout_btn1']"));
		js.executeScript("arguments[0].click();", logoutButton);
	}

	public void clickOnManagerReimbursementTeamReimbursementList()
	{
		
		WaitForLoader();
		QAFWebElement ManagerReimbursementTab = driver.findElement(By.xpath("(//span[@title='Reimbursement'])[2]"));
		action = new Actions(driver);
		action.moveToElement(ManagerReimbursementTab).click().build().perform();
		QAFTestBase.pause(2000);

		
		QAFWebElement TeamReimbursementList = driver.findElement(By.xpath("//a[text()='Teams Reimbursement List']//parent::li[@ng-repeat='subMenu in nav[nav.name] track by $index ']"));
		action = new Actions(driver);
		action.moveToElement(TeamReimbursementList).click().build().perform();
		QAFTestBase.pause(2000);
		
	}
	
	public void clickOnTeamReimbursementList()
	{
		QAFWebElement TitleReimbursementListLink = driver.findElement(By.xpath("//td[contains(text(),'₹2000.00')]//preceding-sibling::td[contains(text(),'Ashish Biradar')]//following-sibling::td[contains(text(),'Wells Fargo Onboarding')]"));
		action = new Actions(driver);
		action.moveToElement(TitleReimbursementListLink).click().build().perform();
		QAFTestBase.pause(2000);
		
	}
	
	public void ClickExpenseManagerTextBox()
	{
		WaitForLoader();
		driver.findElement(By.xpath("//textarea[@class='form-control time_line_comment m-t-15 ng-pristine ng-valid ng-scope ng-empty ng-touched']")).sendKeys("Good");
	}
	
//	//button[contains(text(),'Approve') and @class='btn_prim_1 m-l-10 m-r-10 ng-scope']
	
	public void clickOnReimbursementApproveTab()
	{
		driver.findElement(By.xpath("//button[contains(text(),'Approve') and @class='btn_prim_1 m-l-10 m-r-10 ng-scope']")).click();
	}
	
}
   