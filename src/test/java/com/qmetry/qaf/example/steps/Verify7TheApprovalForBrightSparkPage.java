/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;


public class Verify7TheApprovalForBrightSparkPage extends WebDriverBaseTestPage<WebDriverTestPage>

{
	public static Actions action;

	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
		
	@FindBy(locator = "login.RandR.loc")
	private QAFExtendedWebElement loginRandRTab;
	
	@FindBy(locator = "login.Nominate.loc")
	private QAFExtendedWebElement loginNominateTab;
	
	@FindBy(locator = "login.BrightSpark.loc")
	private QAFExtendedWebElement loginBrightSparkTab;
	
	@FindBy(locator = "login.SelectCard.loc")
	private QAFExtendedWebElement loginSelectCardTab;
	
	@FindBy(locator = "login.Project.loc")
	private QAFExtendedWebElement loginProjectTab;
	
	@FindBy(locator = "login.Nominee.loc")
	private QAFExtendedWebElement loginNomineeTab;
	
	@FindBy(locator = "login.ChallengingSituationsFaced.loc")
	private QAFExtendedWebElement loginChallengingSituationsFacedTab;
	
	@FindBy(locator = "login.SolutionsProvided.loc")
	private QAFExtendedWebElement loginSolutionsProvidedTab;
	
	@FindBy(locator = "login.BenefitsAccrued.loc")
	private QAFExtendedWebElement loginBenefitsAccruedTab;
	
	@FindBy(locator = "login.RationaleForNomination.loc")
	private QAFExtendedWebElement loginRationaleForNominationTab;
	
    @FindBy(locator = "login.Post.loc")
    private QAFExtendedWebElement loginPostTab;
    	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	@FindBy(locator = "login.RRrequests.loc")
	private QAFExtendedWebElement RRrequestsTab;
	
	@FindBy(locator = "login.RewardName.loc")
	private QAFExtendedWebElement RewardNamedTab;
	
	@FindBy(locator = "login.approve.loc")
	private QAFExtendedWebElement approveTab;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
public void WaitForLoader()
  {
  	loader.waitForNotVisible();
  }

public void clickOnRRNaminateAndBrightSpark()
{
	WaitForLoader();
	QAFWebElement loginRandRTab = driver.findElement(By.xpath("(//span[contains(@title,'R & R')]//child::img[contains(@class,'settings_btn1')])[1]"));	
	/*Actions actions = new Actions(driver);
	actions.moveToElement(loginRandRTab).click().build().perform();*/
	QAFTestBase.pause(3000);
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("arguments[0].click();", loginRandRTab);
	
	WaitForLoader();
	QAFWebElement loginNominateTab = driver.findElement(By.xpath("//span[contains(.,'Nominate')]"));
	action = new Actions(driver);
	action.moveToElement(loginNominateTab).click().build().perform();
	QAFTestBase.pause(3000);
	
	WaitForLoader();
	QAFWebElement loginBrightSparkTab = driver.findElement(By.xpath("//a[contains(text(),'Bright Spark')]"));
	action = new Actions(driver);
	action.moveToElement(loginBrightSparkTab).click().build().perform();
	QAFTestBase.pause(4000);

}

//public void clickOnNominateTab()
//{
//	WaitForLoader();
//	QAFWebElement loginNominateTab = driver.findElement(By.xpath("//span[contains(.,'Nominate')]"));
//	action = new Actions(driver);
//	action.moveToElement(loginNominateTab).click().build().perform();
//	QAFTestBase.pause(3000);	
//	
//}
//
//public void clickOnBrightSparkTab()
//{
//	WaitForLoader();
//	QAFWebElement loginBrightSparkTab = driver.findElement(By.xpath("//a[contains(text(),'Bright Spark')]"));
//	Actions actions = new Actions(driver);
//	actions.moveToElement(loginBrightSparkTab).click().build().perform();
//	QAFTestBase.pause(4000);
//	
////	JavascriptExecutor js1 = (JavascriptExecutor) driver;
////	js1.executeScript("arguments[0].scrollIntoView(true);", loginBrightSparkTab);
////	loginBrightSparkTab.waitForVisible();
////	loginBrightSparkTab.click();
//	
//}

public void clickOnSelectCardButton()
{
	
	//loginSelectCardTab.waitForVisible();
//	QAFWebElement loginSelectCardTab = driver.findElement(By.xpath("//div[@class='card_preview display_inline_blk m-r-10 cursor-pointer ng-scope selected_image_border']"));
//	Actions actions = new Actions(driver);
//	actions.moveToElement(loginSelectCardTab).click().build().perform();
	
	
//	WebDriverWait wait2 = new WebDriverWait(driver, 20);
//	wait2.until(ExpectedConditions.elementToBeSelected((By.xpath("//img[@src='https://neststag.infostretch.com:8446/nest_security_testing/nativefw/images/rewardrecognization/Bright_Spark/Card_1.jpg']"))));
//	
	WaitForLoader();
	QAFWebElement loginSelectCardTab = driver.findElement(By.xpath("//img[@src='https://neststag.infostretch.com:8446/nest_security_testing/nativefw/images/rewardrecognization/Bright_Spark/Card_1.jpg']"));
	JavascriptExecutor executor = (JavascriptExecutor)driver;
	executor.executeScript("arguments[0].click();", loginSelectCardTab);
	QAFTestBase.pause(4000);
}

public void clickOnProjectTab()
{
	loginProjectTab.waitForVisible();
	List<WebElement> list = driver.findElements(By.xpath("//div[contains(@class,'multiselect-parent btn-group dropdown-multiselect open')]//button[contains(@type,'button')]"));
	System.out.println(list.size());
	for(int i = 0;i<list.size();i++){
		System.out.println(list.get(i).getText());
		if(list.get(i).getText().equals("Virat Kohali"))
		{
			list.get(i).click();
			break;
		}
		
	}
	loginProjectTab.click();
	
}

public void clickOnNomineeTab()
{
	loginNomineeTab.waitForVisible();
	
	List<WebElement> list = driver.findElements(By.xpath("//div[@class='multi_select_cont leave_status_multiselect width100per m-b-15 pos_rel dropdown']//button[@type='button']"));
	System.out.println(list.size());
	for(int i = 0;i<list.size();i++){
		System.out.println(list.get(i).getText());
		if(list.get(i).getText().equals("Nest Test"))
		{
			list.get(i).click();
			break;
		}
		
	}
	loginNomineeTab.click();
}

public void userEnterTextInAllRequiredFields()
{
	loginChallengingSituationsFacedTab.waitForVisible();
	loginChallengingSituationsFacedTab.sendKeys("NA");

	loginSolutionsProvidedTab.waitForVisible();
	loginSolutionsProvidedTab.sendKeys("NA");
	
	loginBenefitsAccruedTab.waitForVisible();
	loginBenefitsAccruedTab.sendKeys("NA");
	
	loginRationaleForNominationTab.waitForVisible();
	loginRationaleForNominationTab.sendKeys("NA");
	
}

//public void enterTextChallengingSituationsFaced()
//{
//	loginChallengingSituationsFacedTab.waitForVisible();
//	loginChallengingSituationsFacedTab.sendKeys("NA");
//}
//
//public void enterTextSolutionsProvidedTab()
//{
//	loginSolutionsProvidedTab.waitForVisible();
//	loginSolutionsProvidedTab.sendKeys("NA");
//}
//
//public void enterTextBenefitsAccruedTab()
//{
//	loginBenefitsAccruedTab.waitForVisible();
//	loginBenefitsAccruedTab.sendKeys("NA");
//}
//
//public void enterTextRationaleForNominationTab()
//{
//	loginRationaleForNominationTab.waitForVisible();
//	loginRationaleForNominationTab.sendKeys("NA");
//}

public void clickOnPostTab()
{
	loginPostTab.waitForVisible();
	QAFWebElement loginPostTab = driver.findElement(By.xpath("//button[contains(text(),'Post')]"));
	Actions actions = new Actions(driver);
	actions.moveToElement(loginPostTab).click().build().perform();
//	loginPostTab.click();
}

public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}

public void clickOnRRrequestsTab()
{
	
	WaitForLoader();
	QAFWebElement MgrRRTab = driver.findElement(By.xpath("(//span[contains(@title,'R & R')]//child::img[contains(@class,'settings_btn1')])[2]"));
	action = new Actions(driver);
	action.moveToElement(loginNominateTab).click().build().perform();
	QAFTestBase.pause(3000);
	
	QAFWebElement MgrRRRequests = driver.findElement(By.xpath("//a[contains(text(),'R & R Requests')]"));
	action = new Actions(driver);
	action.moveToElement(loginNominateTab).click().build().perform();
	QAFTestBase.pause(3000);
}

public void clickOnRewardNamedTab()
{
	WaitForLoader();
	RewardNamedTab.waitForVisible();
	RewardNamedTab.click();
	QAFTestBase.pause(3000);
}

public void verifyapproveTab()
{
	approveTab.waitForVisible();
	
}

}
   
