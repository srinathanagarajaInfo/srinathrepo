/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify21UIForNewPostPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
	public static JavascriptExecutor js;
	public static Actions action;
	
	@FindBy(locator = "login.Publisher")
	private QAFWebElement loginPublisher;
	
	@FindBy(locator = "login.MyPosts")
	private QAFExtendedWebElement loginMyPosts;
	
	@FindBy(locator = "login.CreateButton")
	private QAFExtendedWebElement loginCreateButton;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.Title")
	private QAFExtendedWebElement loginTitle;
	
	@FindBy(locator = "login.PostUrl")
	private QAFExtendedWebElement loginPostUrl;
	
	@FindBy(locator = "login.ImageUrl")
	private QAFExtendedWebElement loginImageUrl;
	
	@FindBy(locator = "login.LocationDropdown")
	private QAFExtendedWebElement loginLocationDropdown;
	
	@FindBy(locator = "login.CategoryDropdown")
	private QAFExtendedWebElement loginCategoryDropdown;
	
	@FindBy(locator = "login.Description")
	private QAFExtendedWebElement loginDescription;
	
	@FindBy(locator = "login.SubmitButton")
	private QAFExtendedWebElement loginSubmitButton;
	
	@FindBy(locator = "login.BackButton")
	private QAFExtendedWebElement loginBackButton;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
  public void clickOnPublisherMyPosts()
  {
	  {	
		    WaitForLoader();
		    QAFWebElement loginPublisher = driver.findElement(By.xpath("(//span[@title='Publisher' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu'])[2]"));
//			QAFWebElement loginPublisher = driver.findElement("login.Publisher");
			action = new Actions(driver);
			action.moveToElement(loginPublisher).click().build().perform();
			QAFTestBase.pause(2000);
			
			QAFWebElement loginMyPosts = driver.findElement(By.xpath("//a[contains(text(),'My Posts')]"));
//			QAFWebElement loginMyPosts = driver.findElement("login.MyPosts");
			action = new Actions(driver);
			action.moveToElement(loginMyPosts).click().build().perform();
			QAFTestBase.pause(2000);
	

			
		}
  }
  
  public void clickOnCreateMyPosts()
  {
	  WaitForLoader();
//	  QAFWebElement loginCreateButton = driver.findElement("login.CreateButton");
	  QAFWebElement loginCreateButton = driver.findElement(By.xpath("//button[contains(@class,'m-l-10 m-r-10 m-t-15') and text()='Create']"));
	  action = new Actions(driver);
	  action.moveToElement(loginCreateButton).click().build().perform();
	  QAFTestBase.pause(2000);
  }
    
  public void WaitForLoader()
{
	loader.waitForNotVisible();
}

}
   
	


