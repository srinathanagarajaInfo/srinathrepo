/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify17TheFunctionalityOfSubmitBttonWithInvalidDetailsPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	public static Actions action;
	
	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.loader2.loc")
	private QAFExtendedWebElement loader2;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
	    
   }

public void WaitForLoader()
{
	loader.waitForNotVisible();
}

public void clickOnNewExpenseTabWithInvalidCredentials()
{
	WaitForLoader();
	QAFWebElement NewExpenseTab = driver.findElement(By.xpath("//button[text()='New Expense']"));
	action = new Actions(driver);
	action.moveToElement(NewExpenseTab).click().build().perform();
	QAFTestBase.pause(2000);
	
	driver.findElement(By.xpath("//input[@placeholder='Title']")).sendKeys("    ");
//	########### Select Project
	driver.findElement(By.xpath("//tr[contains(@class,'ng-scope')]//td[2]//input[1]")).sendKeys("Nest Test");
	action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();
//	Select  Expense ######
	driver.findElement(By.xpath("//tr[contains(@class,'ng-scope')]//td[3]//input[1]")).sendKeys("1111");
	action = new Actions(driver);
	action.sendKeys(Keys.RETURN).perform();

//	Select ExpenseAmountTextTab
	driver.findElement(By.xpath("//div[contains(@class,'input-group')]//input[contains(@placeholder,'')]")).sendKeys("1945");

}

}
   
	


