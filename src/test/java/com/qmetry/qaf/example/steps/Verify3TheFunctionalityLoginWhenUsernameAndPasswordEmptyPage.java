/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.apache.commons.lang.Validate;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify3TheFunctionalityLoginWhenUsernameAndPasswordEmptyPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	@FindBy(locator = "login.UserNameRequired")
	private QAFWebElement UserNameRequired;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	

public void verifyUserNameRequiredErrorMessage()
{
	UserNameRequired.waitForVisible();
	boolean UserNameRequired = driver.findElement("login.UserNameRequired").verifyPresent();
	Validate.isTrue(UserNameRequired, "UserNameRequired is visible");
	
}

}
   
	


