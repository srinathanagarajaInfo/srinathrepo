
package com.qmetry.qaf.example.steps;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify5TheFunctionalityOfTheViewAllLinkPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
   public static Actions action;
   public static JavascriptExecutor js;
   
   @FindBy(locator = "login.ViewAllLink")
   private QAFWebElement ViewAlllink;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }

  
  public void ClickOnTravelAndTravelRequestTab()
  {
	 
	   
		QAFWebElement Travel = driver.findElement(By.xpath("(//span[@title='Travel'])[2]"));
		action.moveToElement(Travel).click().build().perform();
		QAFTestBase.pause(2000);
		
		QAFWebElement TravelRequestTab = driver.findElement(By.xpath("(//a[contains(text(),'Travel Requests')])[3]"));
		action.moveToElement(TravelRequestTab).click().build().perform();
		QAFTestBase.pause(2000);

  }
  
  public void userClickOnViewAllTab()
  {
	  ViewAlllink.waitForVisible();
	  ViewAlllink.click();
	  
  }


}
   
	


