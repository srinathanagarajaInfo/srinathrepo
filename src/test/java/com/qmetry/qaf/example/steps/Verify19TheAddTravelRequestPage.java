/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.interactions.Actions;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify19TheAddTravelRequestPage extends WebDriverBaseTestPage<WebDriverTestPage>
{
	
	public static JavascriptExecutor js;
	public static Actions action;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
  
  public void clickOnTravelAndMyTravelRequests()
  {
	  {	
		    WaitForLoader();
			QAFWebElement TravelTab = driver.findElement(By.xpath("(//span[@class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu' and @title='Travel'])[1]"));
			action = new Actions(driver);
			action.moveToElement(TravelTab).click().build().perform();
			QAFTestBase.pause(2000);
	
			QAFWebElement MyTravelRequests = driver.findElement(By.xpath("//a[contains(text(),'My Travel Requests')]"));
			action = new Actions(driver);
			action.moveToElement(MyTravelRequests).click().build().perform();
			QAFTestBase.pause(2000);
			
		}
  }
  
  public void clickNewTravelRequest()
  {
	  
	  WaitForLoader();
	  QAFWebElement NewTravelRequest = driver.findElement(By.xpath("//button[contains(text(),'New Travel Request')]"));
	  action = new Actions(driver);
	  action.moveToElement(NewTravelRequest).click().build().perform();
	  QAFTestBase.pause(2000);
  }

  public void WaitForLoader()
  {
	loader.waitForNotVisible();
  }


}
   
	


