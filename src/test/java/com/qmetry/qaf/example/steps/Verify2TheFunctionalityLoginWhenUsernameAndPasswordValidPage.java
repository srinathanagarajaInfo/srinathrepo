/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;
import org.apache.commons.lang.Validate;
import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify2TheFunctionalityLoginWhenUsernameAndPasswordValidPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.loader2.loc")
	private QAFExtendedWebElement loader2;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	
	@FindBy(locator = "login.HomePageLogo")
	private QAFWebElement HomePageLogo;
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
  
 
public void clickOnLoginPage(String username, String password) {
	
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();     
   }

public void WaitForLoader()
{
	loader.waitForNotVisible();
}

public void verifyTheHomePageLogo()
{
	HomePageLogo.waitForVisible();
	boolean HomePageLogo = driver.findElement("login.HomePageLogo").verifyPresent();
	Validate.isTrue(HomePageLogo, "HomePage is visible");
	

}


public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}

public void clickOnTravelTab()
{
	loader.waitForNotVisible();
	traveltabTab.waitForVisible();
	traveltabTab.click();
}

public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}


}
   
	


