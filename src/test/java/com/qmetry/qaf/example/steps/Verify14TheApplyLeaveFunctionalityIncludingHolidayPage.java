/**
 * 
 */
/**
 * @author Rahil.saxena
 *
 */
package com.qmetry.qaf.example.steps;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.TestPage;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class Verify14TheApplyLeaveFunctionalityIncludingHolidayPage extends WebDriverBaseTestPage<WebDriverTestPage>
{

	public static JavascriptExecutor js;
	public static Actions action;
	
	@FindBy(locator = "login.username.loc")
	private QAFWebElement usernametxt;
	
	@FindBy(locator = "login.password.loc")
	private QAFExtendedWebElement passwordtxt;
	
	@FindBy(locator = "login.loginButton.loc")
	private QAFExtendedWebElement loginButtonbtn;
	
	@FindBy(locator = "login.loader.loc")
	private QAFExtendedWebElement loader;
	
	@FindBy(locator = "login.managertab.loc")
	private QAFExtendedWebElement managertabTab;
	
	@FindBy(locator = "login.traveltab.loc")
	private QAFExtendedWebElement traveltabTab;
	
	@FindBy(locator = "login.viewtab.loc")
	private QAFExtendedWebElement viewtabTab;
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		driver.get("/");
		QAFTestBase.pause(3000);
	}
	
  @Override
  public boolean isPageActive(PageLocator loc, Object... args) {
	
	  return false;
   }
	
  public void clickOnLoginPage(String username, String password) {
	    usernametxt.waitForVisible();
	    usernametxt.sendKeys(username);
	    passwordtxt.waitForVisible();
	    passwordtxt.sendKeys(password);
	    loginButtonbtn.waitForVisible();
	    loginButtonbtn.click();
	    loader.waitForNotVisible();
	    
   }

//  ###############
  
  public void clickOnLeaveMenuBarAndApplyLeave()
  {
	  {
		    WaitForLoader();
			QAFWebElement LeaveTab = driver.findElement(By.xpath("(//span[@title='Leave' and @class='fa-stack fa-lg pull-left left_menu_minimize_icon fa-stack-menu'])[1]"));
			action = new Actions(driver);
			action.moveToElement(LeaveTab).click().build().perform();
			QAFTestBase.pause(2000);
	
			QAFWebElement ApplyLeaveTab = driver.findElement(By.xpath("//a[contains(text(),'Apply Leave')]//parent::li[@class='ng-scope left_menu_close_section']"));
			action = new Actions(driver);
			action.moveToElement(ApplyLeaveTab).click().build().perform();
			QAFTestBase.pause(2000);
			
		}
  }
  
  public void selectTheFromAndToDate()
  {
	  
	  {
		  driver.findElement(By.xpath("//input[@id='startDate_apply']")).sendKeys("13-August-2018");
		  WaitForLoader();
//		  String FromDate = "13-August-2018";
//		  QAFWebElement elementFrom = 
//		  QAFWebElement elementTo= 
		 driver.findElement(By.xpath("//input[@id='endDate_apply']")).sendKeys("17-August-2018");
		 WaitForLoader();
//		 String ToDate = "17-August-2018";
		  
//		  js = ((JavascriptExecutor)driver);
//		  js.executeScript("arguments[0].setAttribute('value', '"+FromDate+"');",elementFrom);
//		  js.executeScript("arguments[0].setAttribute('value', '"+ToDate+"');",elementTo);
		  
	  }
  }
  
  public void selectTheReasonForLeave()
  {
	  
//	  driver.findElement(By.xpath("//div[@class='btn-group width100per m-b-15 pos_rel dropdown open']//button[@id='btn-append-to-body']")).click();
//	  
//	  List<WebElement> list = driver.findElements(By.xpath("//ul[@class='dropdown-menu width100per']//li//a"));
//		System.out.println(list.size());
//		for(int i = 0;i<list.size();i++){
//			System.out.println(list.get(i).getText());
//			if(list.get(i).getText().equals("Family Function"))
//			{
//				list.get(i).click();
//				break;
//			}
//	
//		}
//		
////	    js = ((JavascriptExecutor)driver);
//	  js.executeScript("arguments[0].click();", selectTheReasonForLeave);
   action = new Actions(driver);
   QAFWebElement SelectDropdown = driver.findElement(By.xpath("(//button[@id='btn-append-to-body'])[1]"));
   action.moveToElement(SelectDropdown).click().build().perform();
   QAFTestBase.pause(2000);
	
	  
	QAFWebElement selectTheReasonForLeave = driver.findElement(By.xpath("//button[@id='btn-append-to-body']//following-sibling::ul[@class='dropdown-menu width100per']//child::li[@ng-click='vm.onReasonSelect(leaveReason)']//child::a[contains(text(),'Family Function')]"));
	
	action.moveToElement(selectTheReasonForLeave).click().build().perform();
	QAFTestBase.pause(2000);  
  }
  
  public void userShouldSelectTheDay()
  {
	
	driver.findElement(By.xpath("//input[@value='full_day']//following-sibling::span[@class='label_for_radio_btn']")).click();
	
  }
  
  public void userClickOnApplyButton()
  {
	  
	driver.findElement(By.xpath("//button[@class='btn_prim_1']")).click();
  }
   
//  ###############
  
  public void WaitForLoader()
{
	loader.waitForNotVisible();
}
  

public void clickOnManagerView()
{
	managertabTab.waitForVisible();
	managertabTab.click();
}

public void clickOnTravelTab()
{
	loader.waitForNotVisible();
	traveltabTab.waitForVisible();
	traveltabTab.click();
}
public void clickOnlViewTab()
{
	viewtabTab.waitForVisible();
	viewtabTab.click();
}

}
   
	


