package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify10DetailOfBrightSparkPage;

public class Verify9DetailOfBrightSparkTest2 extends WebDriverBaseTestPage<WebDriverTestPage>{

	Verify10DetailOfBrightSparkPage login = new Verify10DetailOfBrightSparkPage();
	
	
	@QAFTestStep(description = "user should launch url")
	public void UserShouldLaunchUrl() {
		login.launchPage(null);
	}
	
	@QAFTestStep(description = "user Click R and R")
	public void userclickOnRandRTest()
	{
		login.clickOnRandRTab();
	}
	
	@QAFTestStep(description = "user click On Nominate Tab")
	public void userclickOnNominateTab()
	{
		login.clickOnNominateTab();
	}
	
	@QAFTestStep(description = "user click On BrightSpark button")
	public void userclickOnBrightSparkbutton()
	{
		login.clickOnBrightSparkTab();
	}
	
	@QAFTestStep(description = "user click On Select Card Tab")
	public void userclickOnSelectCardTab() 
	{
	 
	  login.clickOnSelectCardButton();	
	}
	
	@QAFTestStep(description = "user click On Nominee Tab")
	public void userclickOnNomineeTab(){
		
		login.clickOnNomineeTab();
		
	}
	
	
	@QAFTestStep(description = "user click On Project Tab")
	public void userclickOnProjectTab()
	{
		login.clickOnProjectTab();
		
		
	}
	
	@QAFTestStep(description = "user Enter NA in required fields")
	public void userEnterNAinrequiredfields() {
		
		login.enterTextChallengingSituationsFaced();
		login.enterTextSolutionsProvidedTab();
		login.enterTextBenefitsAccruedTab();
		login.enterTextRationaleForNominationTab();
		
	}
	
	@QAFTestStep(description = "user click On Post Tab")
	public void userclickOnPostTab() 
	{
		login.clickOnPostTab();
	}
	
	@QAFTestStep(description = "user click on logoutButton")
	public void userclickOnLogoutButton()
	{
		login.clickOnLogoutButton();
		
	}
	
	// Manager usability

	@QAFTestStep(description = "user should launch url")
	public void UserShouldLaunchUrl1() {
		login.launchPage(null);
	}
	
	@QAFTestStep(description = "user Click R and R tab Button")
	public void userClickRandRTabButton()
	{
		login.clickOnRandRTab();
	}
	
	
	@QAFTestStep(description = "user click on R and R requests Tab Button")
	public void userClickOnRandRRequestsTabButton()
	{
	    login.clickOnRRrequestsTabButton();	
	    
	}

//	##########
	
	
	@QAFTestStep(description = "user click on BrightSparkTab where status rejected")
	public void userClickOnBrightSparkTabWhereStatusRejected()
	{
		login.clickOnBrightSparkRejectTab();
	}
	
	@QAFTestStep(description = "user should see blocks are visible or not")
	public void usershouldSeeBlocksAreVisibleOrNot()
	{
		login.verifyHRStatusLable();
		login.verifyManagerLable();
		login.verifyManagerStatusLable();
		login.verifyNominatedByLable();
		login.verifyNomineeLable();
		login.verifyPostedDateLable();
		login.verifyProjectLable();
		login.verifyRewardNameLable();
		
	} 
	

	
	@QAFTestStep(description = "user should verify Reward card image visible")
	public void userShouldVerifyRewardCardImageVisible()
	{
		login.clickOnBrightSparkTab();
	}
	
	@QAFTestStep(description = "user should verify Back Button visible")
	public void userShouldVerifyBackButtonVisible()
	{
		login.clickOnBrightSparkTab();
	}
	
	
	@QAFTestStep(description = "user click on logoutButton")
	public void userclickOnLogoutButton1()
	{
		login.clickOnLogoutButton();
		
	}
	
	// Delivery Manager Usability
	
	@QAFTestStep(description = "user should launch url")
	public void UserShouldLaunchUrl2() {
		login.launchPage(null);
	}
	
	@QAFTestStep(description = "user enter valid credential like username as {0} and password as {1}")
	public void userLoginWithManagerID1(String username, String password)
	{
		login.loginWithManagerID(username, password);
		
	}
	
	@QAFTestStep(description = "user Click R and R tab")
	public void userclickOnRandRTab()
	{
		login.clickOnRandRTab();
	}
	
	@QAFTestStep(description = "user click on RRMenuBar")
	public void userClickOnRRMenuBar()
	{
		login.clickOnRRMenuBar();
	}
	@QAFTestStep(description = "user click on R and R requests Tab")
	public void userClickRandRrequestsTab()
	{
	login.clickOnRRrequestsTabButton();	
	}

	@QAFTestStep(description = "user click on BrightSparkTab where manager status pending")
	public void userClickOnBrightSparkTabWhereManagerStatusPending()
	{
	  login.clickOnBrightSparkPendingTab(); 
	}

//	@QAFTestStep(description = "user click on approveTab")
//	public void userClickOnApproveTab()
//	{
//		login.clickOnApproveTab();
//	}
	
	@QAFTestStep(description = "user click on BrightSparkTab where HR status pending")
	public void userClickOnBrightSparkTabWhereHRStatusPending()
	{
		login.clickOnBrightSparkHRApproveTab();
	}
	
	@QAFTestStep(description = "user click On MyRRandMyRewards")
	public void userClickOnMyRRandMyRewards()
	{
		login.clickOnMyRRandMyRewards();
	}
	
	@QAFTestStep(description = "user click on secTest")
	public void userClickOnsecTest(){
		{
			login.clickOnSecTestTab();
		}
	}
	
	@QAFTestStep(description = "user click on approveTab")
	public void userClickOnApproveTab()
	{
		login.clickOnApproveTab();
	}
	
//	user clicks on RandR tab
	@QAFTestStep(description="user clicks on RandR tab")
	public void userClicksOnRandRTab()
	{
		login.clickOnRandRTab();
	}
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
}
