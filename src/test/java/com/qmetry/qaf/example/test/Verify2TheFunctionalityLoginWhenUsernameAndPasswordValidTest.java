package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify2TheFunctionalityLoginWhenUsernameAndPasswordValidPage;


public class Verify2TheFunctionalityLoginWhenUsernameAndPasswordValidTest extends WebDriverBaseTestPage<WebDriverTestPage>{
		
	Verify2TheFunctionalityLoginWhenUsernameAndPasswordValidPage login = new Verify2TheFunctionalityLoginWhenUsernameAndPasswordValidPage();
	

	@QAFTestStep(description="Wait for loader")
	public void waitForLoader()
	{
		login.WaitForLoader();
	}
	
	@QAFTestStep(description="user given username as {0} and password as {1}")
	public void userGivenUsernameAsAndPasswordAs(String username, String password) {
		
		login.clickOnLoginPage(username, password);
	}

	@QAFTestStep(description = "click on managerView tab")
	public void clickOnManagerViewTab()
	{
		login.clickOnManagerView();
	}
	
	@QAFTestStep(description = "click on traveltab")
	public void clickOnTravelTab()
	{
		login.clickOnTravelTab();
	}

	@QAFTestStep(description = "click on viewtab")
	public void clickOnViewTab()
	{
		login.clickOnlViewTab();
	}

	@QAFTestStep(description="user should verify Logo of HomePage is present")
	public void userShouldVerifyLogoOfHomePageIsPresent()
	{
		login.verifyTheHomePageLogo();
	}
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
