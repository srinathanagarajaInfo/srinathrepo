package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify17TheFunctionalityOfSubmitBttonWithInvalidDetailsPage;

public class Verify17TheFunctionalityOfSubmitBttonWithInvalidDetailsTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify17TheFunctionalityOfSubmitBttonWithInvalidDetailsPage login = new Verify17TheFunctionalityOfSubmitBttonWithInvalidDetailsPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
	
	@QAFTestStep(description = "user click On NewExpenseTab WithInvalidCredentials")
	public void userClickOnNewExpenseTabWithInvalidCredentials()
	{
		login.clickOnNewExpenseTabWithInvalidCredentials();
	}
	
	@QAFTestStep(description = "user should verify the error")
	public void userShouldVerifyTheError()
	{
		driver.findElement(By.xpath("(//div[contains(text(),'This is a required field.')]//parent::div[@class='tooltip ng-isolate-scope bottom fade in'])[1]")).verifyPresent();
	}
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
