package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify3TheFunctionalityLoginWhenUsernameAndPasswordEmptyPage;



public class Verify3TheFunctionalityLoginWhenUsernameAndPasswordEmptyTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify3TheFunctionalityLoginWhenUsernameAndPasswordEmptyPage login = new Verify3TheFunctionalityLoginWhenUsernameAndPasswordEmptyPage();

	@QAFTestStep(description="user should verify UserNameRequiredErrorMessage is present")
	public void userShouldVerifyUserNameRequiredErrorMessageIsPresent()
	{
		login.verifyUserNameRequiredErrorMessage();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
