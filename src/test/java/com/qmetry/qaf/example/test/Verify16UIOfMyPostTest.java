package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify16UIOfMyPostPage;


public class Verify16UIOfMyPostTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify16UIOfMyPostPage login = new Verify16UIOfMyPostPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		
		login.launchPage(null);
	}
	
//	######################
	
	@QAFTestStep(description="user click on Publisher and My Posts")
	public void userClickOnPublisherAndMyPosts()
	{
	   login.clickOnPublisherAndMyPosts();
	}
	
	@QAFTestStep(description="user should verify the Home Publisher locator")
	public void userShouldVerifyTheHomePublisherLocator()
	{
	   boolean HomePublisherLocator = driver.findElement(By.xpath("//span[@class='page_title_text' and contains(text(),'Home')]")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the FromAndToDate lable")
	public void userShouldVerifyTheFromAndToDateLable()
	{
	   boolean FromDatePicker = driver.findElement(By.xpath("//div[@class='row']//div[@class='row']//div[1]//div[1]//div[2]//input[1]")).verifyPresent();
	   boolean ToDatePicker = driver.findElement(By.xpath("//div[@class='row']//div[2]//div[1]//div[2]//input[1]")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the KeyWord Textfield")
	public void userShouldVerifyTheKeyWordTextfield()
	{
	   boolean KeywordTextFields = driver.findElement(By.xpath("//input[@type='text' and @placeholder='Enter Keyword']")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the Category dropdown")
	public void userShouldVerifyTheCategoryDropdown()
	{
	 boolean CategoryDropdown = driver.findElement(By.xpath("//button[@type='button']//parent::div[@class='multiselect-parent btn-group dropdown-multiselect']")).verifyPresent();  
	}
	
	@QAFTestStep(description="user should verify the SearchAndReset buttons")
	public void userShouldVerifyTheSearchAndResetButtons()
	{
	boolean SearchButton =  driver.findElement(By.xpath("//button[@class='btn_prim_1 m-r-10' and text()='Search']")).verifyPresent(); 
	boolean ResetButton =  driver.findElement(By.xpath("//button[@class='btn_sec_1 m-r-10' and text()='Reset']")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the column details")
	public void userShouldVerifyTheColumnDetails()
	{
	 boolean Title = driver.findElement(By.xpath("")).verifyPresent();
	 boolean Category = driver.findElement(By.xpath("")).verifyPresent();
	 boolean PostedOn = driver.findElement(By.xpath("")).verifyPresent();
	 boolean Action = driver.findElement(By.xpath("")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the filter icon")
	public void userShouldVerifyTheFilterIcon()
	{
	 boolean FilterIcon =  driver.findElement(By.xpath("//button[@class='btn_filter1 pull-right filter_btn_position_in_bread_crumb']")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the Pagination lable")
	public void userShouldVerifyThePaginationLable()
	{
	 boolean PaginationLable = driver.findElement(By.xpath("//ul[@class='pagination-sm pagination ng-isolate-scope ng-not-empty ng-valid']")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the EditAndDelete icons")
	public void userShouldVerifyTheEditAndDeleteIcons()
	{
	boolean EditButton = driver.findElement(By.xpath("//div[@title='Edit']")).verifyPresent();
	boolean DeleteButton = driver.findElement(By.xpath("//div[@title='Delete']")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the Create Post button")
	public void userShouldVerifyTheCreatePostButton()
	{
	 boolean CreatPost = driver.findElement(By.xpath("//button[@class='btn_prim_1  m-l-10 m-r-10 m-t-15' and text()='Create']")).verifyPresent();
	}
	
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
