package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify14TheApplyLeaveFunctionalityIncludingHolidayPage;

public class Verify14TheApplyLeaveFunctionalityIncludingHolidayTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify14TheApplyLeaveFunctionalityIncludingHolidayPage login = new Verify14TheApplyLeaveFunctionalityIncludingHolidayPage();

	@QAFTestStep(description="user should launchs url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
		
	@QAFTestStep(description="user click on LeaveMenuBar and ApplyLeave")
	public void userClickOnLeaveMenuBarAndApplyLeave()
	{
	
		login.clickOnLeaveMenuBarAndApplyLeave();
	}
	
	@QAFTestStep(description="user should select the Reason for Leave")
	public void userShouldSelectTheReasonForLeave()
	{
	
		login.selectTheReasonForLeave();
	}

	@QAFTestStep(description="user should select the FromAndToDate")
	public void userShouldSelectTheFromAndToDate()
	{
		login.selectTheFromAndToDate();
	}
	
	@QAFTestStep(description="user should select the day")
	public void userShouldSelectTheDay()
	{
	
		login.userShouldSelectTheDay();
	}
	
	@QAFTestStep(description="user click on apply button")
	public void userClickOnApplyButton()
	{
	
		login.userClickOnApplyButton();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
