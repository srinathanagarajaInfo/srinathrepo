package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify11FunctionalityOfExpenseRequestApprovalPage;

public class Verify11FunctionalityOfExpenseRequestApprovalTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify11FunctionalityOfExpenseRequestApprovalPage login = new Verify11FunctionalityOfExpenseRequestApprovalPage();

	@QAFTestStep(description="user should launchs url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
	
	@QAFTestStep(description="user click on ReimbursementMenuBar My Expense List New Expense Submit")
	public void userClickOnReimbursementMenuBarMyExpenseListNewExpenseSubmit()
	{
	
		login.clickOnExpenseReimbursementMyExpenseList();
	}
	
	@QAFTestStep(description = "user click on NewExpenseTab")
	public void userclickOnNewExpenseTab()
	{
		login.clickOnNewExpenseTab();
	}
//	
//	@QAFTestStep(description = "user click on SelectProjectTextTab")
//	public void userclickOnSelectProjectTextTab()
//	{
//		login.clickOnSelectProjectTextTab();
//	}
//	
//	@QAFTestStep(description = "user click on SelectExpenseTextTab")
//	public void userclickOnSelectExpenseTextTab()
//	{
//		login.clickOnSelectExpenseTextTab();
//	}
	
	
	@QAFTestStep(description = "user click on ExpenseSubmitButton")
	public void userClickOnExpenseSubmitButton()
	{
		login.clickOnExpenseSubmitButton();	
		QAFTestBase.pause(4000);
		
	}
	
	@QAFTestStep(description = "user logoutButton")
	public void userLogoutButton()
	{
		login.logoutButton();
	}
	
	@QAFTestStep(description = "user click on ManagerReimbursementTeamReimbursementList")
	public void userClickOnManagerReimbursementTeamReimbursementList()
    {
	  login.clickOnManagerReimbursementTeamReimbursementList();	
     }
	
	@QAFTestStep(description = "user Click on ExpenseManagerTextBox")
	public void userClickOnExpenseManagerTextBox()
	{
		login.ClickExpenseManagerTextBox();
	}
	
	@QAFTestStep(description = "user click on ReimbursementApprove Tab")
	public void userClickOnReimbursementApproveTab()
	{
		login.clickOnReimbursementApproveTab();
	}
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
