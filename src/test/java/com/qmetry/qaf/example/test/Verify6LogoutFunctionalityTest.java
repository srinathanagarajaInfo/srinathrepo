package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify6LogoutFunctionalityPage;

public class Verify6LogoutFunctionalityTest extends WebDriverBaseTestPage<WebDriverTestPage>{
		
	Verify6LogoutFunctionalityPage login = new Verify6LogoutFunctionalityPage();
	
	@QAFTestStep(description = "user click on logout Button")
    public void userClickOnLogoutButton()
    {
	 QAFTestBase.pause(2000);
	 login.clickOnLogoutButton();   
    }
	
	@QAFTestStep(description = "user Validate LogoutButton")
    public void userValidateLogoutButton()
    {
	 QAFTestBase.pause(2000);
	 login.clickOnLogoutButton();   
    }
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
