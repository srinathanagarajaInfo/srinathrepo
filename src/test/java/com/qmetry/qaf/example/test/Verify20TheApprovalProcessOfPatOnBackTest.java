package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify20TheApprovalProcessOfPatOnBackPage;

public class Verify20TheApprovalProcessOfPatOnBackTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify20TheApprovalProcessOfPatOnBackPage login = new Verify20TheApprovalProcessOfPatOnBackPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
	
	@QAFTestStep(description="user click on RRPatOnBackBar")
	public void userClickOnRRPatOnBackBar()
	{
		login.clickOnRRPatOnBackBar();
	}
	
	@QAFTestStep(description = "user Enter Key Contribution by Nominee")
	public void userEnterKeyContributionByNominee()
	{
		login.userEnterKeyContributionByNominee();
	}
	
	@QAFTestStep(description = "user click On Select PatOnBackCardButton")
	public void userClickOnSelectPatOnBackCardButton()
	{
		login.clickOnSelectPatOnBackCardButton();
	}
	
	@QAFTestStep(description = "user should verify Naminee name")
	public void usershouldverifyNamineeName()
	{
	boolean NamineeName = driver.findElement(By.xpath("(//td[@data-title='Nominee' and text()='Virat Kohali'])[1]")).verifyPresent();
	}
	

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
