package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify21UIForNewPostPage;
import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;


public class Verify21UIForNewPostTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify21UIForNewPostPage login = new Verify21UIForNewPostPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		
		login.launchPage(null);
	}
	
	@QAFTestStep(description="user click on Publisher and MyPosts")
	public void userClickOnPublisherAndMyPosts()
	{
	   login.clickOnPublisherMyPosts();
	}
	
	@QAFTestStep(description="user click on Create Button")
	public void userClickOnCreateButton()
	{
	 login.clickOnCreateMyPosts(); 
	 waitForPageToLoad();
	}
	
//	######################
	
	@QAFTestStep(description="user should verify titlePostUrlImageUrl")
	public void userShouldVerifyTitlePostUrlImageUrl()
	{
//	   boolean Title = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter Title')]//parent::div[@class='m-b-15']")).verifyPresent();
//	   boolean PostUrl = driver.findElement(By.xpath("//input[contains(@placeholder,'Enter URL')]//parent::div[@class='m-b-15']")).verifyPresent();
//	   boolean ImageUrl = driver.findElement(By.xpath("//input[@id='imageUrl']//parent::div[@class='m-b-15']")).verifyPresent();
//	  
	   boolean loginTitle = driver.findElement("login.Title").verifyPresent();
	   boolean loginPostUrl= driver.findElement("login.PostUrl").verifyPresent();
	   boolean loginImageUrl = driver.findElement("login.ImageUrl").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify select Location dropdown")
	public void userShouldVerifySelectLocationdropdown()
	{
//	   boolean LocationDropdown = driver.findElement(By.xpath("//div[contains(@class,'col-md-12 col-lg-12 m-l-12')]//div[contains(@class,'row')]//div[contains(@class,'row')]//div[1]//div[2]//div[1]//div[1]//button[1]")).verifyPresent();
	   
	   boolean loginLocationDropdown = driver.findElement("login.LocationDropdown").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify select Category dropdown")
	public void userShouldVerifySelectCategoryDropdown()
	{
//	 boolean CategoryDropdown = driver.findElement(By.xpath("//div[contains(@class,'col-md-12 col-lg-12 m-l-12')]//div[contains(@class,'row')]//div[contains(@class,'row')]//div[2]//div[2]//div[1]//div[1]//button[1]")).verifyPresent();
	 
	 boolean loginCategoryDropdown = driver.findElement("login.CategoryDropdown").verifyPresent();
	 
	}
	
//	#####
	@QAFTestStep(description="user should verify Description")
	public void userShouldVerifyJourneyFromAndTo()
	{
//	boolean Description =  driver.findElement(By.xpath("//textarea[contains(@placeholder,'Enter Description')]")).verifyPresent();
	
	boolean loginDescription = driver.findElement("login.Description").verifyPresent();
	}
	@QAFTestStep(description="user should verify submit button")
	public void userShouldVerifySubmitButton()
	{
//	boolean SubmitButton =  driver.findElement(By.xpath("//button[contains(@type,'submit')]")).verifyPresent();
	
	boolean loginSubmitButton = driver.findElement("login.SubmitButton").verifyPresent();
	
	}
	
	@QAFTestStep(description="user should verify Back button")
	public void userShouldVerifyBackButton()
	{
//	boolean BackButton =  driver.findElement(By.xpath("//button[contains(text(),'Back')]")).verifyPresent();
	
	boolean loginBackButton = driver.findElement("login.BackButton").verifyPresent();
	}
	
//	######################
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
