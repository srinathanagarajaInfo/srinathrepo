package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify13TheApplyLeaveFunctionalitySingleDayLeavePage;

public class Verify13TheApplyLeaveFunctionalitySingleDayLeaveTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify13TheApplyLeaveFunctionalitySingleDayLeavePage login = new Verify13TheApplyLeaveFunctionalitySingleDayLeavePage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
