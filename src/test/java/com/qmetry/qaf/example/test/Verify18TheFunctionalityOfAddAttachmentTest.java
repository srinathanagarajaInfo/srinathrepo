package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify18TheFunctionalityOfAddAttachmentPage;

public class Verify18TheFunctionalityOfAddAttachmentTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify18TheFunctionalityOfAddAttachmentPage login = new Verify18TheFunctionalityOfAddAttachmentPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
	
	@QAFTestStep(description="user click on AttachBrowseButton")
	public void userClickOnAttachBrowseButton() throws Exception
	{
		login.clickOnAttachBrowseButton();
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
