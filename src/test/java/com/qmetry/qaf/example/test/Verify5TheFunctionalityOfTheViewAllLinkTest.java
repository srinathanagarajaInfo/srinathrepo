package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify5TheFunctionalityOfTheViewAllLinkPage;


public class Verify5TheFunctionalityOfTheViewAllLinkTest extends WebDriverBaseTestPage<WebDriverTestPage>{
		
	Verify5TheFunctionalityOfTheViewAllLinkPage login = new Verify5TheFunctionalityOfTheViewAllLinkPage();
	
	@QAFTestStep(description = "user click on ViewAll tab")
	public void userClickOnViewAllTab()
	{
		login.userClickOnViewAllTab();
		
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
