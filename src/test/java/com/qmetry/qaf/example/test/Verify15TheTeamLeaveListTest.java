package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify15TheTeamLeaveListPage;

public class Verify15TheTeamLeaveListTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify15TheTeamLeaveListPage login = new Verify15TheTeamLeaveListPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		login.launchPage(null);
		
	}
	
	@QAFTestStep(description="user click on LeaveAndTeamLeaveList")
	public void userClickOnLeaveAndTeamLeaveList()
	{
		login.clickOnLeaveAndTeamLeaveList();
	}
	
	@QAFTestStep(description="user should verify the Employee ID")
	public void userShouldVerifyTheEmployeeID()
	{
	  boolean EmployeeID= driver.findElement(By.xpath("//th[contains(text(),'Employee Name (ID)')]")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify the Employee Name")
	public void userShouldVerifyTheEmployeeName()
	{
		boolean EmployeeName= driver.findElement(By.xpath("//th[contains(text(),'Employee Name (ID)')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Applied Date")
	public void userShouldVerifyTheAppliedDate()
	{
		boolean AppliedDate= driver.findElement(By.xpath("//span[@class='ng-scope'][contains(text(),'Applied Date')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Employee Type")
	public void userShouldVerifyTheEmployeeType()
	{
		boolean Type= driver.findElement(By.xpath("//th[contains(text(),'Type')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Leave Duration")
	public void userShouldVerifyTheLeaveDuration()
	{
		boolean LeaveDuration = driver.findElement(By.xpath("//th[@class='width_80' and contains(text(),'Leave Duration')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Leave Date")
	public void userShouldVerifyTheLeaveDate()
	{
		boolean LeaveDate= driver.findElement(By.xpath("//span[@class='ng-scope'][contains(text(),'Leave Date')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Status")
	public void userShouldVerifyTheLeaveStatus()
	{
		boolean Status= driver.findElement(By.xpath("//th[contains(text(),'Status')]")).verifyPresent();	
	}
	@QAFTestStep(description="user should verify the Leave Reason")
	public void userShouldVerifyTheLeaveReason()
	{
		boolean LeaveReason= driver.findElement(By.xpath("//th[contains(text(),'Leave Reason')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Project")
	public void userShouldVerifyTheProject()
	{
		boolean Project= driver.findElement(By.xpath("//th[@class='width_120' and contains(text(),'Project')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Managers comment")
	public void userShouldVerifyTheManagersComment()
	{
		boolean ManagerComment= driver.findElement(By.xpath("//th[contains(text(),'Manager's Comment')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Actions")
	public void userShouldVerifyTheActions()
	{
		boolean Actions= driver.findElement(By.xpath("//th[contains(text(),'Actions')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Back dated Leave")
	public void userShouldVerifyTheBackDatedLeave()
	{
		boolean BacDatedLeave= driver.findElement(By.xpath("//th[contains(text(),'Back Dated Leave')]")).verifyPresent();
	}
	@QAFTestStep(description="user should verify the Submit and ApproveAll button")
	public void userShouldVerifyTheSubmitAndApproveAllButton()
	{
		boolean ApproveAllButton= driver.findElement(By.xpath("//button[contains(text(),'Approve All')]")).verifyPresent();
	}


	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
