package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify19TheAddTravelRequestPage;


public class Verify19TheAddTravelRequestTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify19TheAddTravelRequestPage login = new Verify19TheAddTravelRequestPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		
		login.launchPage(null);
	}

	@QAFTestStep(description="user click on travel and My travel requests")
	public void userClickOnTravelAndMyTravelRequests()
	{
	   login.clickOnTravelAndMyTravelRequests();
	}
	
	@QAFTestStep(description="user click on New travel request")
	public void userClickOnNewTravelRequest()
	{
	   login.clickNewTravelRequest();
	   login.WaitForLoader();
	}
	
//	######################
	
//	######################
	
	@QAFTestStep(description="user should verify Purpose Of Travel")
	public void userShouldVerifyPurposeOfTravel()
	{
//	   boolean PurposeOfTravel = driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and text()=' Purpose of Travel']")).verifyPresent();
	   boolean PurposeOfTravel = driver.findElement("login.PurposeOfTravel").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify Type")
	public void userShouldVerifyType()
	{
//	   boolean TypeText = driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and text()='Type']")).verifyPresent();
	   boolean TypeText = driver.findElement("login.TypeText").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify Travel Type")
	public void userShouldVerifyTravelType()
	{
//	 boolean TravelType = driver.findElement(By.xpath("//div[contains(text(),'Travel Type')]")).verifyPresent();
	 boolean TravelType = driver.findElement("login.TravelType").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify JourneyFrom")
	public void userShouldVerifyJourneyFromAndToDate()
	{
//	boolean JourneyFrom =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Journey From')]")).verifyPresent();
	boolean JourneyFrom = driver.findElement("login.JourneyFrom").verifyPresent();
	}
//	#####
	@QAFTestStep(description="user should verify JourneyTo")
	public void userShouldVerifyJourneyFromAndTo()
	{
//	boolean JourneyTo =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Journey To')]")).verifyPresent();
	boolean JourneyTo = driver.findElement("login.JourneyTo").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify Journey StartDate and EndDate")
	public void userShouldVerifyJourneyStartDateAndEndDate()
	{
	boolean StartDate = driver.findElement("login.StartDate").verifyPresent();
	boolean EndDate = driver.findElement("login.EndDate").verifyPresent();	
//	boolean StartDate =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Journey Start Date')]")).verifyPresent();
//	boolean EndDate =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Journey End Date')]")).verifyPresent();
	}
	
	@QAFTestStep(description="user should verify ClientProjectName")
	public void userShouldVerifyClientProjectName()
	{
//	boolean ClientProjectName =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),' Client / Project Name')]")).verifyPresent();
	boolean ClientProjectName = driver.findElement("login.ClientProjectName").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify CabHotelBooking")
	public void userShouldVerifyCabHotelBooking()
	{
//	boolean CabHotelBooking =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Hotel Booking Required')]")).verifyPresent();
	boolean CabHotelBooking = driver.findElement("login.CabHotelBooking").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify CashAdavance Required")
	public void userShouldVerifyCashAdavanceRequired()
	{
//	boolean CashAdavanceRequired =  driver.findElement(By.xpath("//div[@class='form_text_normal m-b-6' and contains(text(),'Need Cash In Advance')]")).verifyPresent();
	boolean CashAdavanceRequired = driver.findElement("login.CashAdavanceRequired").verifyPresent();
	}
	
	@QAFTestStep(description="user should verify IDAttachment")
	public void userShouldVerifyIDAttachment()
	{
//	boolean IDAttachment =  driver.findElement(By.xpath("//div[@class='form_text_normal m-t-15 ' and contains(text(),'ID Proof')]")).verifyPresent();
	boolean IDAttachment = driver.findElement("login.IDAttachment").verifyPresent();
	}
	
	@QAFTestStep(description="user should MoreInformation")
	public void userShouldMoreInformation()
	{
	boolean MoreInformation = driver.findElement("login.MoreInformation").verifyPresent();	
//	boolean MoreInformation =  driver.findElement(By.xpath("//div[contains(text(),'Extra Information')]")).verifyPresent();
	}

//	######################
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
