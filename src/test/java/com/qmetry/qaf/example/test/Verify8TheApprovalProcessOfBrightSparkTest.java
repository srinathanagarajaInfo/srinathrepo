package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify8TheApprovalProcessOfBrightSparkPageNew;

public class Verify8TheApprovalProcessOfBrightSparkTest extends WebDriverBaseTestPage<WebDriverTestPage>{
		
	Verify8TheApprovalProcessOfBrightSparkPageNew login = new Verify8TheApprovalProcessOfBrightSparkPageNew();
	
	@QAFTestStep(description = "user click on RRNaminate and Bright Spark")
    public void userClickOnRRNaminateAndBrightSpark()
    {
		login.clickOnRRNaminateAndBrightSpark();
    }
	
	@QAFTestStep(description = "user click on clickOnSelectCardButton")
    public void userClickOnClickOnSelectCardButton()
    {
		login.clickOnSelectCardButton();
    }
	
	@QAFTestStep(description = "user click on clickOnProjectTab")
    public void userClickOnClickOnProjectTab()
    {
       login.clickOnProjectTab();
    }
	
	@QAFTestStep(description = "user click on NomineeTab")
    public void userClickOnNomineeTab()
    {
        login.clickOnNomineeTab();
    }
	
	@QAFTestStep(description = "user enter Text in all required fields")
    public void userEnterTextInAllRequiredFields()
    {
       login.userEnterTextInAllRequiredFields();
    }
	
	@QAFTestStep(description = "user click on PostTab")
    public void userClickOnPostTab()
    {
       login.clickOnPostTab();
    }
	
	@QAFTestStep(description = "user click on RR and RRRequests")
    public void userClickOnRRAndRRRequests()
    {
       login.clickOnRRrequestsTab();
    }

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
