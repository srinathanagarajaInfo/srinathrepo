package com.qmetry.qaf.example.test;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify4TheFunctionalityOfTravelRequestSectionPage;


public class Verify4TheFunctionalityOfTravelRequestSectionTest extends WebDriverBaseTestPage<WebDriverTestPage>{
		
	Verify4TheFunctionalityOfTravelRequestSectionPage login = new Verify4TheFunctionalityOfTravelRequestSectionPage();


	@QAFTestStep(description = "user click on Travel and TravelRequestTab")
	public void userClickOnTravelAndTravelRequestTab()
	{
		login.ClickOnTravelTab();
		login.clickOnTravelRequests();
		
	}
	
	@QAFTestStep(description = "user click on Travel Purpose")
	public void userClickOnTravelPurpose()
	{
		login.clickOnTravelPurpose();
		
	}

	@QAFTestStep(description = "user should verify Approve Reject and Back buttons")
	public void userShouldVerifyApproveRejectAndBackButtons()
	{
		login.userShouldVerifyApproveRejectBackButtons();
		
	}

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
