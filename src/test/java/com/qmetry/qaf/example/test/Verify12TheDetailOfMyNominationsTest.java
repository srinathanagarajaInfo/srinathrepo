package com.qmetry.qaf.example.test;

import org.openqa.selenium.By;

import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.example.steps.Verify12TheDetailOfMyNominationsPage;

public class Verify12TheDetailOfMyNominationsTest extends WebDriverBaseTestPage<WebDriverTestPage>{
			
	Verify12TheDetailOfMyNominationsPage login = new Verify12TheDetailOfMyNominationsPage();

	@QAFTestStep(description="user should launch url")
	public void userShouldLlaunchUrl() {
		
		login.launchPage(null);
	}
//	######################
	
	@QAFTestStep(description="user click on RRandMyNomination bar")
	public void userClickOnRRandMyNominationBar()
	{
		login.clickOnRRandMyNominationBar();
		
////		Page title verification
//		
		
		
		boolean MyNominations = driver.findElement("//div[contains(text(),'My Nominations')]").verifyPresent();
		
//		System.out.println(MyNominations);
//	    Assert.assertEquals(MyNominations, "My Nominations");
	    
	
////	    Homebreadcrumheading1 verification
	    
	    boolean Homebreadcrumheading1 = driver.findElement(By.xpath("//span[@class='page_title_subtext']")).verifyPresent();
//	    System.out.println(Homebreadcrumheading1);
//	    Assert.assertEquals(Homebreadcrumheading1, "R & R /  My R & R");
	    
	    
//	    Columns verification
	    
	    boolean Nominee = driver.findElement(By.xpath("//span[text()='Nominee(s)']")).verifyPresent();
//	    System.out.println("Page title is"+ Nominee);
//	    Assert.assertEquals(Nominee, "Nominee(s)");
	    
	    boolean Reward =  driver.findElement(By.xpath("//span[text()='No. of Reward(s)']")).verifyPresent();
//	    System.out.println("Page title is"+ Reward);
//	    Assert.assertEquals(Reward, "No. of Reward(s)");
	   
	    boolean PostedDate =  driver.findElement(By.xpath("//span[text()='Posted Date']")).verifyPresent();
//	    System.out.println("Page title is"+ PostedDate);
//	    Assert.assertEquals(PostedDate, "Posted Date");
	   
	    boolean ManagerStatus =  driver.findElement(By.xpath("//span[text()='Manager Status']")).verifyPresent();
//	    System.out.println("Page title is"+ ManagerStatus);
//	    Assert.assertEquals(ManagerStatus, "Manager Status");
	   
	   boolean HRStatus =  driver.findElement(By.xpath("//span[text()='HR Status']")).verifyPresent();
	   System.out.println("Page title is"+ HRStatus);
//	   Assert.assertEquals(HRStatus, "HR Status");
	   
	   
	   
//	   #############   Pagination Tab
	   
	  boolean Pagination =  driver.findElement(By.xpath("//ul[@class='pagination-sm pagination ng-isolate-scope ng-not-empty ng-valid']")).verifyPresent();
	   
	}
	
//	######################  HR Status
	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		
	}
	
}
	
